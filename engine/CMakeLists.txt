cmake_minimum_required(VERSION 3.20)

project(engine)
set(CMAKE_CXX_STANDARD 20)


INCLUDE(FindPkgConfig)

PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
include_directories(${GLEW_INCLUDE_DIRS})


set(ENGINE_SOURCE
src/engine.cpp
src/main.cpp
)

add_library(engine STATIC ${ENGINE_SOURCE})
target_link_libraries(engine ${SDL2_LIBRARIES} GL ${GLEW_LIBRARIES})
