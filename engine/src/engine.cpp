#include <engine.hpp>

// Shaders
// Vertex shader code
const char* vertRawShader = "#version 330 core\n"
"layout (location 0) in vec3 aPos;\n"
"void main() {\n"
"  gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"};\0";
// Fragment shader code
const char* fragRawShader = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main() {\n"
"  FragColor = vec4(0.0f, 1.0f, 0.0f, 1.0f);\n"
"};\n\0";

GLfloat verts[] = {
  0.0f, 0.5f, 1.0f,
  0.5f, -0.5f, 1.0f,
  -0.5f, -0.5f, 1.0f
};

GLuint vertShader;
GLuint fragShader;
GLuint shaderProgram;
GLuint VBO;
GLuint VAO;

Engine::Engine::Engine() {
  // Init event system
  m_event = new SDL_Event();
  // Init SDL layer
  SDL_Init(SDL_INIT_VIDEO);
  // Set doublebuffer mode
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  // Create window
  m_window = SDL_CreateWindow("Engine", 0, 0, 
			      m_width, m_height,
			      SDL_WINDOW_OPENGL);
  // Create Opengl context from window
  m_glContext = SDL_GL_CreateContext(m_window);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK)
  {
    std::cout << "Failed to initialize GLEW" << std::endl;
  }


  vertShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertShader, 1, &vertRawShader, NULL);
  glCompileShader(vertShader);

  fragShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragShader, 1, &fragRawShader, NULL);
  glCompileShader(fragShader);

  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertShader);
  glAttachShader(shaderProgram, fragShader);

  glLinkProgram(shaderProgram);

  glDeleteShader(vertShader);
  glDeleteShader(fragShader);


  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);


  // Set clear color
  glClearColor(0.0f, 0.2f, 0.2f, 1.0f);
  // Resize buffer
  glViewport(0, 0, m_width, m_height);
};

Engine::Engine::~Engine() {

  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  glDeleteProgram(shaderProgram);

  // Destroy window
  SDL_DestroyWindow(m_window);
  // Destroy context
  SDL_GL_DeleteContext(m_glContext);
  // Deinit SDL layer
  SDL_Quit();
};

int
Engine::Engine::loop() {
  // Get events
  SDL_PollEvent(m_event);
  // Process current event
  switch (m_event->type) {
  case SDL_QUIT:
    return 0;
    break;
  };
  // Clear Screen
  glClear(GL_COLOR_BUFFER_BIT);


  glUseProgram(shaderProgram);
  glBindVertexArray(VAO);
  glDrawArrays(GL_TRIANGLES, 0, 3);


  // Swap buffers of window
  SDL_GL_SwapWindow(m_window);
  return 1;
};
